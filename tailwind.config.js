const appTypography = require("./config/typography");
const { palette, brand } = require("./config/colors");


module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      screens:{
        'laptop-medium': '1217px',
      },
      spacing: {
        5.5: "-32px",
        4.5: "-20px",
        8.5: "34px",
        17: "72px",
        18: "78px",
        19: "84px",
        22: "82px",
        23: "92px",
        33: "136px",
        35: "140px",
        42: "164px",
        45: "185px",
        49: "200px",
        49.5: "235px",
        50: "574px",
        85: "372px",
        85.5: "397px",
        86: "421px",
        88: "429px",
        90: "479px",
        150: "510px",
        157: "575px",
        162: "605px",
        165: "622px",
        170: "632px",
        180: "690px",
        190: "768px",
        310: "1039px",
        311: "1040px",
        350: "1440px",
      },
      minWidth: {
        '0': '0',
        '1/4': '250px',
        '1/2': '400px',
        '1/5': '150px',
        'full': '100%',
       },
    },
    colors: palette,
    fontFamily: {
      ...appTypography.fontFamily,
    },
    fontSize: {
      ...appTypography.headings.fontSize,
      ...appTypography.body.fontSize,
    },
  },
  variants: {
    extend: {},
  },
  plugins: [
    require('@tailwindcss/line-clamp'),
  ],
};
