/* eslint-disable @next/next/no-img-element */
import React, { FC } from "react";
import { PrimaryButton } from "../core/PrimaryButton";

interface BannerSectionProps {
  className?: string | undefined;
}

export const BannerSection: FC<BannerSectionProps> =
  ({}: BannerSectionProps) => {
    const viewServices = () => {
      window.location.href = "#service";
    };

    return (
      <section 
      className="pt-22 bg-gray-50">
        <div className="md:flex lg:h-190 ">
          <div className="lg:w-11/12 pl-2 pt-2 sm:pt-2 md:pt-14 laptop-medium:pt-28 lg:mt-14 md:pl-20 lg:pl-49">
            <h1 className="font-manrope font-semibold text-h2 md:text-h1 lg:text-h1 text-black-500 pb-6 lg:pb-6">
              We make software that work with humans.
            </h1>
            <p className="font-inter font-normal text-base text-black-500 pb-8">
              We believe that communication is key to creating solutions that
              communicate. That’s why we always work in close collaboration with
              clients big and small. These are some of the companies we’ve had
              the opportunity to work
            </p>
            <PrimaryButton title="View services" onClick={viewServices} />
          </div>
          <div className=" relative float-right z-10 lg:w-full">
            <img
              className="lg:min-w-1/2 md:relative lg:absolute top-8.5 right-0 hidden sm:inline lg:flat:right lg:w-165 md:h-full lg:h-180 sm:pt-10 md:pt-0"
              src="/images/bannerPanel.png"
              alt="Doted Image"
            />
            <img
              className="lg:min-w-1/4 absolute top-18 right-0 px-2 sm:px-2 md:px-2 lg:px-0"
              src="/images/banner1-image1.png"
              alt="Board Discussion "
            />

            <img
              className="lg:min-w-1/5 relative sm:absolute md:absolute lg:absolute top-72 sm:top-96  md:top-56 lg:top-85.5 right-0 px-2 sm:px-2 md:px-2 lg:px-0"
              src="/images/banner1-image2.png"
              alt="Technical Discussion"
            />
          </div>
        </div>
      </section>
    );
  };

BannerSection.defaultProps = {
  className: undefined,
};

export default BannerSection;
