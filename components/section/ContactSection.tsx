import React, { FC, useState, ChangeEvent, useRef, useEffect } from "react";
import { PrimaryButton } from "../core/PrimaryButton";
import emailjs from "emailjs-com";

interface ContactSectionProps {
  className?: string | undefined;
}

export const ContactSection: FC<ContactSectionProps> =
  ({}: ContactSectionProps) => {
    const [state, setState] = useState({
      disabled: false,
      email: "",
      errors: {
        firstName: "",
        lastName: "",
        email: "",
        message: "",
      },
      formVisibility: false,
      firstName: "",
      lastName: "",
      loading: false,
      message: "",
      errorMessage: {
        status: false,
        title: "",
        message: "",
      },
      successMessage: {
        status: false,
        title: "",
        message: "",
      },
    });

    const contactFormRef = useRef(null);

    const handleContactForm = () => {
      const formVisibility = state.formVisibility;
      let newStatus = true;

      formVisibility ? (newStatus = false) : (newStatus = true);
      setState({
        ...state,
        formVisibility: newStatus,
      });
    };

    const validateFormFields = () => {
      const errors = state.errors;

      switch (state.firstName) {
        case "":
          errors.firstName = "First Name required!";
          break;
        default:
          errors.firstName = "";
      }

      switch (state.lastName) {
        case "":
          errors.lastName = "Last Name required!";
          break;
        default:
          errors.lastName = "";
      }

      switch (state.email) {
        case "":
          errors.email = "Email required!";
          break;
        default:
          errors.email = "";
      }

      switch (state.message) {
        case "":
          errors.message = "Message required!";
          break;
        default:
          errors.message = "";
      }

      let valid = true;
      Object.values(errors).forEach((val) => val.length > 0 && (valid = false));
      return valid;
    };

    const onHandleChange = (e: ChangeEvent<HTMLInputElement>) => {
      setState({
        ...state,
        [e.target.name]: e.target.value,
      });
    };

    const onAreaChange = (e: any) => {
      setState({
        ...state,
        message: e.target.value,
      });
    };

    const submitForm = (event: React.FormEvent<HTMLFormElement>) => {
      event.preventDefault();
      setState({
        ...state,
        successMessage: {
          status: false,
          title: "",
          message: "",
        },
        errorMessage: {
          status: false,
          title: "",
          message: "",
        },
      });
      if (validateFormFields()) {
        setState({
          ...state,
          loading: true,
          disabled: true,
        });
        if (contactFormRef.current != null) {
          emailjs
            .sendForm(
              "service_iyzw9cb",
              "template_jw9evjk",
              contactFormRef.current,
              "user_MYsW0hGRWiuNjoKiT2wh9"
            )
            .then(
              (result) => {
                console.log(result.text);

                setState({
                  ...state,
                  loading: false,
                  disabled: false,
                  successMessage: {
                    status: true,
                    title: "Success",
                    message: "Email sent successfully.",
                  },
                });
              },
              (error) => {
                setState({
                  ...state,
                  loading: false,
                  disabled: false,
                  errorMessage: {
                    status: true,
                    title: "Error",
                    message: "Email not sent successfully, Please try again.",
                  },
                });
              }
            );
        }
      }
    };

    return (
      <section
        className="pt-20 sm:pt-20 md:pt-24 lg:pt-24 bg-gray-50"
      >
        <div className="px-2 w-full sm:w-150 mx-auto pb-20">
          <text className="font-manrope font-normal text-link text-blue-500 pb-4">
            Got an idea?
          </text>
          <h2 className="font-manrope font-semibold text-h2 text-black-500 pb-6">
            Let’s talk about how we can drive growth for your project.
          </h2>

          <div className="pt-8 pb-7">
            <PrimaryButton
              title="Contact us"
              onClick={() => handleContactForm()}
            />
          </div>

          <div className={state.formVisibility ? "block" : "hidden"}>
            <form
              className="w-full max-w-lg"
              onSubmit={submitForm}
              ref={contactFormRef}
            >
              <div className="flex flex-wrap -px-3 pb-6">
                <div className="w-full md:w-1/2 px-3 pb-6 md:pb-0">
                  <label
                    className="font-manrope block uppercase tracking-wide text-gray-700 text-xs font-bold pb-2"
                    htmlFor="grid-first-name"
                  >
                    First Name
                  </label>
                  <input
                    className="appearance-none block w-full bg-mischka-100 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
                    id="grid-first-name"
                    name="firstName"
                    type="text"
                    placeholder="first name"
                    value={state.firstName}
                    onChange={onHandleChange}
                  />
                  {state.errors.firstName.length > 0 && (
                    <p className="text-red-500 text-xs italic">
                      {state.errors.firstName}
                    </p>
                  )}
                </div>
                <div className="w-full md:w-1/2 px-3 ">
                  <label
                    className="font-manrope block uppercase tracking-wide text-gray-700 text-xs font-bold pb-2"
                    htmlFor="grid-last-name"
                  >
                    Last Name
                  </label>
                  <input
                    className="appearance-none block w-full bg-mischka-100 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
                    id="grid-last-name"
                    name="lastName"
                    type="text"
                    placeholder="last name"
                    value={state.lastName}
                    onChange={onHandleChange}
                  />
                  {state.errors.lastName.length > 0 && (
                    <p className="text-red-500 text-xs italic">
                      {state.errors.lastName}
                    </p>
                  )}
                </div>
              </div>
              <div className="flex flex-wrap -px-3 pb-6">
                <div className="w-full px-3">
                  <label
                    className="font-manrope block uppercase tracking-wide text-gray-700 text-xs font-bold pb-2"
                    htmlFor="grid-email"
                  >
                    Email
                  </label>
                  <input
                    className="appearance-none block w-full bg-mischka-100 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                    id="grid-email"
                    name="email"
                    type="email"
                    placeholder="email"
                    value={state.email}
                    onChange={onHandleChange}
                  />
                  {state.errors.email.length > 0 && (
                    <p className="text-red-500 text-xs italic">
                      {state.errors.email}
                    </p>
                  )}
                </div>
              </div>
              <div className="flex flex-wrap -px-3 pb-6">
                <div className="w-full px-3">
                  <label
                    className="font-manrope block uppercase tracking-wide text-gray-700 text-xs font-bold pb-2"
                    htmlFor="grid-message"
                  >
                    Message
                  </label>
                  <textarea
                    className="appearance-none block w-full bg-mischka-100 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                    id="grid-message"
                    name="message"
                    placeholder="message"
                    rows={6}
                    value={state.message}
                    onChange={onAreaChange}
                  />
                  {state.errors.message.length > 0 && (
                    <p className="text-red-500 text-xs italic">
                      {state.errors.message}
                    </p>
                  )}

                  {state.successMessage.status ? (
                    <div className="bg-green-100 border-l-4 border-green-500 text-green-700 p-4" role="alert">
                    <p className="font-bold">{state.successMessage.title}</p>
                    <p>{state.successMessage.message}</p>
                  </div>
                  ) : state.errorMessage.status ? (
                    <div className="bg-red-100 border-l-4 border-red-500 text-red-700 p-4" role="alert">
                    <p className="font-bold">{state.errorMessage.title}</p>
                    <p>{state.errorMessage.message}</p>
                  </div>
                  ) : null}

                  <div className="float-right pt-10">
                    <PrimaryButton
                      title="Send"
                      type="submit"
                      loading={state.loading}
                      disabled={state.disabled}
                    />
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </section>
    );
  };

ContactSection.defaultProps = {
  className: undefined,
};

export default ContactSection;
