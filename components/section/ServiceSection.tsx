import React, { FC, useState } from "react";
import { SectionInfo } from "../core/SectionInfo";
import { ServiceCard } from "../core/ServiceCard";

interface ServiceSectionProps {
  className?: string | undefined;
}

export const ServiceSection: FC<ServiceSectionProps> =
  ({}: ServiceSectionProps) => {
    const [services] = useState([
      {
        id: 1,
        level: [
          {
            id: 1,
            title: "Software Development Services",
            description:
              "Get a qualitative product out with less bugs and reports. It’s easy start your project now.",
          },
          {
            id: 2,
            title: "Mobile Application Services",
            description:
              "Get a qualitative product out with less bugs and reports. It’s easy start your project now.",
          },
          {
            id: 3,
            title: "Software Security Services",
            description:
              "Get a qualitative product out with less bugs and reports. It’s easy start your project now.",
          },
        ],
      },
      {
        id: 2,
        level: [
          {
            id: 1,
            title: "Web Development Services",
            description:
              "Get a qualitative product out with less bugs and reports. It’s easy start your project now.",
          },
          {
            id: 2,
            title: "Microsoft Stack",
            description:
              "Get a qualitative product out with less bugs and reports. It’s easy start your project now.",
          },
          {
            id: 3,
            title: "Quality Assurance",
            description:
              "Get a qualitative product out with less bugs and reports. It’s easy start your project now.",
          },
        ],
      },
    ]);

    return (
      <section className="pt-20 sm:pt-20 md:pt-24 lg:pt-24">
        <SectionInfo
          title="SERVICES"
          header="Bespoke solutions tailored to your business"
          description="Our visionary developers use state-of-the-art technologies to make
        your vision come to life. It’s going to be smooth as butter."
        />
        <div className="lg:w-310 mx-auto">
          {services.map((serviceLevel) => (
            <div key={serviceLevel.id} className="sm:flex">
              {serviceLevel.level.map((service) => (
                <ServiceCard
                  key={service.id}
                  title={service.title}
                  description={service.description}
                />
              ))}
            </div>
          ))}
        </div>
      </section>
    );
  };

ServiceSection.defaultProps = {
  className: undefined,
};

export default ServiceSection;
