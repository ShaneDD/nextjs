import React, { FC, useState } from "react";
import { TeamMemberItem } from "../core/TeamMemberItem";
import { PrimaryButton } from "../core/PrimaryButton";

interface CareerSectionProps {
  className?: string | undefined;
}

export const CareerSection: FC<CareerSectionProps> =
  ({}: CareerSectionProps) => {
    const [members] = useState([
      { url: "/images/p1.png" },
      { url: "/images/p2.png" },
      { url: "/images/p3.png" },
      { url: "/images/p4.png" },
      { url: "/images/p1.png" },
      { url: "/images/p2.png" },
      { url: "/images/p3.png" },
      { url: "/images/p4.png" },
      { url: "/images/p1.png" },
      { url: "/images/p2.png" },
      { url: "/images/p3.png" },
      { url: "/images/p4.png" },
    ]);

    const redirectToSection = () =>{
      window.location.href = "#contact";
    }

    return (
      <section 
      className="pt-20 sm:pt-20 md:pt-24 lg:pt-24">
        <div className="px-2 w-full sm:w-150 mx-auto pb-5">
          <text className="font-manrope font-normal text-link text-blue-500 pb-4">
            CAREERS
          </text>
          <h2 className="font-manrope font-semibold text-h2 text-black-500 pb-6">
            We are always looking for talented and passionate individuals
          </h2>
          <p className="font-inter font-normal text-base text-black-500">
            We tackle the most exciting and complex problems out there. Your job
            is to solve them in meaningful, future-forward ways, with a team of
            some of the sharpest minds around
          </p>

          <div className="pt-8">
            <PrimaryButton title="Join the team" onClick={redirectToSection} />{" "}
          </div>

          <div className="w-85 pt-14 mx-auto sm:mx-0 --item-center">
            <div className="grid grid-cols-2 sm:grid-cols-3 md:grid-cols-4 ml-4.5 ">
              {members.map((member, i) => (
                <div key={i}>
                  <TeamMemberItem imageUrl={member.url} />
                </div>
              ))}
            </div>
          </div>
        </div>
      </section>
    );
  };

CareerSection.defaultProps = {
  className: undefined,
};

export default CareerSection;
