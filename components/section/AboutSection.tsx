/* eslint-disable @next/next/no-img-element */
import React, { FC } from "react";
import { SectionInfo } from "../core/SectionInfo";

interface AboutSectionProps {
  className?: string | undefined;
}

export const AboutSection: FC<AboutSectionProps> = ({}: AboutSectionProps) => {

  return (
    <section
      className="inline-block mt-72 sm:mt-0 pt-20 sm:pt-20 md:pt-24 lg:pt-24"
    >
      <SectionInfo
        title="ABOUT US"
        header="Hey, we’re taprobane"
        description="We’re a digital product and UX agency in San Francisco. We do
          strategy, design, and development across all platforms. Scroll down to
          see why we are the elites."
      />
        <img className="w-screen" src="/images/MaskGroup.png" alt="Taprobanes Office" />
    </section>
  );
};

AboutSection.defaultProps = {
  className: undefined,
};

export default AboutSection;
