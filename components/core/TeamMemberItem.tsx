import React, { FC } from "react";
import Image from "next/image";

interface TeamMemberItemProps {
  className?: string | undefined;
  imageUrl: string;
}

export const TeamMemberItem: FC<TeamMemberItemProps> = ({
  imageUrl,
}: TeamMemberItemProps) => {

  return (
    <div className="rounded-full flex items-center justify-center --img-drop-shadow">
        <Image width="112" height="112" src={imageUrl} alt="Team Member" />
    </div>
  );
};

TeamMemberItem.defaultProps = {
  className: undefined,
  imageUrl: undefined,
};

export default TeamMemberItem;
