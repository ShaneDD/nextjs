import React, { FC } from "react";

interface ServiceCardProps {
  className?: string | undefined;
  description: string;
  title: string;
}

export const ServiceCard: FC<ServiceCardProps> = ({
  description,
  title,
}: ServiceCardProps) => {
  return (
    <div className="p-4 border border-solid border-mischka-500 m-2 ">
      <p className="font-manrope font-semibold text-base text-black-500 w-4/6">
        {title}
      </p>
      <p className="font-inter font-normal text-quote text-black-500 pt-6">
        {description}
      </p>
    </div>
  );
};

ServiceCard.defaultProps = {
  className: undefined,
  description: undefined,
  title: undefined,
};

export default ServiceCard;
