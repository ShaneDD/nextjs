import React, { FC } from "react";
import Image from "next/image";

interface FooterProps {
  className?: string | undefined;
}

export const Footer: FC<FooterProps> = ({}: FooterProps) => {
  return (
    <div className="w-full lg:h-88 bg-mirage-500 px-2 sm:px-2 md:px-5 laptop-medium:px-5 lg:px-2 pb-10 lg:pb-0">
      <div className="lg:w-311 mx-auto pt-10 sm:pt-10 md:pt-20">
        <div className="w-full">
          <Image
            src="/images/logo-white.png"
            alt="Logo"
            width="136"
            height="50"
          />
        </div>
        <div>
          <div className=" pt-8 lg:flex ">
            <div className="lg:pr-35">
              <p className="font-inter font-normal text-quote text-white-100 lg:w-86">
                We’re a bunch of dreamers, thinkers, designers, coders &
                caffeine-addicts who have a burning desire to build stuff that
                matters.
              </p>
            </div>
            <div className="lg:w-90  pt-8 md:pt-8  lg:pt-0  flex  font-manrope font-semibold text-md text-white-100">
              <div className="grid sm:flex">
                <div className="pr-16">
                  <div className="pb-6 md:whitespace-nowrap">
                    <a href="#home">Home</a>
                  </div>
                  <div className="pb-6 md:whitespace-nowrap ">
                    <a href="#career">About us</a>
                  </div>
                  <div className="pb-6 md:whitespace-nowrap">
                    <a href="#contact">Contact us</a>
                  </div>
                </div>
                <div className="pr-16">
                  <div className="pb-6 md:whitespace-nowrap ">
                    <a href="#service">Services</a>
                  </div>
                  <div className="pb-6 md:whitespace-nowrap">
                    <a href="#home">Careers</a>
                  </div>
                </div>
              </div>
              <div>
                <div className="pb-6 md:whitespace-nowrap">
                  <p>Contact</p>
                </div>
                <div className="pb-6 md:whitespace-nowrap">
                  <p>+94 71 793 1735</p>
                </div>
                <div className="pb-6 md:whitespace-nowrap">
                  <p>hello@taprobane.io</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="pt-10 inline-block  lg:flex">
          <div>
            <p className="font-inter font-normal text-link text-white-100 pb-4">
              7 Charles Place, Col 03, Sri Lanka
            </p>
            <p className="font-inter font-medium text-xs text-white-100 pb-10 sm:pb-10 md:pb-0 ">
              © 2021 Taprobane, Terms Privacy policy
            </p>
          </div>
          <div>
            <div className="sm:flex  lg:pl-162 pt-5  lg:pt-0">
              <a href="#" className="mr-4">
                <Image
                  width="24"
                  height="24"
                  src="/images/facebook.png"
                  alt="Facebook"
                />
              </a>
              <a href="#" className="mr-4">
                <Image
                  width="24"
                  height="24"
                  src="/images/linkedin.png"
                  alt="Linkedin"
                />
              </a>
              <a href="#">
                <Image
                  width="24"
                  height="24"
                  src="/images/twitter.png"
                  alt="Twitter"
                />
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

Footer.defaultProps = {
  className: undefined,
};

export default Footer;
