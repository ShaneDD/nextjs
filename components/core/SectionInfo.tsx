import React, { FC } from "react";

interface SectionInfoProps {
  className?: string | undefined;
  description?: string | undefined;
  header: string;
  title: string;
}

export const SectionInfo: FC<SectionInfoProps> = ({
  description,
  header,
  title,
}: SectionInfoProps) => {
  return (
    <div className="px-2 w-full sm:w-150 mx-auto pb-6 md:pb-20">
      <text className="font-manrope font-normal text-link text-blue-500 pb-4">
        {title}
      </text>
      <h2 className="font-manrope font-semibold text-h2 text-black-500 pb-6">
        {header}
      </h2>
      <p className="font-inter font-normal text-base text-black-500">
        {description}
      </p>
    </div>
  );
};

SectionInfo.defaultProps = {
  className: undefined,
  description: undefined,
  header: undefined,
  title: undefined,
};

export default SectionInfo;
