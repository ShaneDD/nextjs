import React, { FC } from "react";
import ClipLoader from "react-spinners/ClipLoader";

interface PrimaryButtonProps {
  className?: string | undefined;
  disabled?: boolean;
  loading?: boolean;
  onClick?: any;
  title: string;
  type?: "button" | "submit" | "reset";
}

export const PrimaryButton: FC<PrimaryButtonProps> = ({
  disabled,
  loading,
  onClick,
  title,
  type,
}: PrimaryButtonProps) => {
  return (
    <button
      type={type}
      className="bg-blue-500 rounded-sm h-12 px-8 text-white-50"
      onClick={onClick}
      disabled={disabled}
    >
      {loading ? (
        <div className="mt-2">
          <ClipLoader color="white" loading={true} size={26} />{" "}
        </div>
      ) : (
        title
      )}
    </button>
  );
};

PrimaryButton.defaultProps = {
  className: undefined,
  disabled: false,
  loading: false,
  onClick: undefined,
  title: undefined,
  type: "button",
};

export default PrimaryButton;
