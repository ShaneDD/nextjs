const { palette } = require("./colors");

module.exports = {
  fontSources: ["Google Fonts"],

  fontStack: [
    {
      name: "DM Serif Display",
      cssClass: "text-serif",
      link: "https://fonts.google.com/specimen/DM+Serif+Display",
    },
    {
      name: "Inter",
      cssClass: "text-sans",
      link: "https://fonts.google.com/specimen/Inter",
    },
    {
      name: "IBM Plex Mono",
      cssClass: "text-mono",
      link: "https://fonts.google.com/specimen/IBM+Plex+Mono",
    },
  ],

  fontFamily: {
    sans: ['"Inter"', "sans-serif"],
    serif: ['"DM Serif Display"', "serif"],
    heading: ['"Inter"', "sans-serif"],
    mono: ['"IBM Plex Mono"', "monospace"],
    code: ['"IBM Plex Mono"', "monospace"],
    manrope: "Manrope",
    inter: "Inter",
  },

  headings: {
    fontFamily: "manrope",

    fontSize: {
      display: [
        "80px",
        {
          label: "Display",
          lineHeight: "90px",
          letterSpacing: "-2px",
        },
      ],
      h1: [
        "66px",
        {
          label: "Heading",
          lineHeight: "74px",
        },
      ],
      h2: [
        "42px",
        {
          label: "Heading",
          lineHeight: "54px",
        },
      ],
      h3: [
        "28px",
        {
          label: "Heading",
          lineHeight: "40px",
          letterSpacing: "-0.5px",
        },
      ],
      h4: [
        "20px",
        {
          label: "Heading",
          lineHeight: "28px",
          letterSpacing: "-0.5px",
        },
      ],
      sub: [
        "12px",
        {
          label: "Sub Heading",
          lineHeight: "12px",
          textTransform: "uppercase",
          letterSpacing: "5%",
        },
      ],
    },

    fontWeight: {
      display: 800,
      h1: 600,
      h2: 700,
      h3: 700,
      h4: 600,
      sub: 600,
    },
  },

  body: {
    fontFamily: ['"Inter", sans-serif', '"IBM Plex Mono", monospace'],
    fontSize: {
      base: [
        "18px",
        {
          label: "Base",
          lineHeight: "28px",
        },
      ],
      xs: [
        "13px",
        {
          label: "Text Extra-Small",
          lineHeight: "13px",
        },
      ],
      sm: [
        "16px",
        {
          label: "Text Small",
          lineHeight: "16px",
        },
      ],
      md: [
        "18px",
        {
          label: "Text Medium",
          lineHeight: "18px",
        },
      ],
      lg: [
        "24px",
        {
          label: "Text Large",
          lineHeight: "36px",
        },
      ],
      quote: [
        "16px",
        {
          label: "Quote",
          lineHeight: "24px",
        },
      ],
      code: [
        "14px",
        {
          label: "Code",
          lineHeight: "20px",
          letterSpacing: "-0.6px",
        },
      ],
      link: [
        "16px",
        {
          label: "Link",
          lineHeight: "16px",
        },
      ],
    },
    fontWeight: {
      base: 400,
      code: 500,
      lg: 400,
      md: 400,
      quote: 600,
      sm: 500,
      xs: 500,
    },
  },

  // colors: {
  //   base: palette.N[800],
  //   heading: palette.N[900],
  //   sub: palette.N[400],
  //   link: palette.N[800],
  //   linkHover: palette.B.base,
  // },
};
